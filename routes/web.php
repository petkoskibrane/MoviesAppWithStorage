<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/showmovieslist')->name('show-movies')->uses('MovieController@showMovies');

Route::get('/showAddMovieForm')->name('add-movie-form')->uses('MovieController@showAddMovieForm');

Route::post('/addNewMovie')->name('add-movie')->uses('MovieController@addMovie');

Route::get('/details/{id}')->name('movie-details')->uses('MovieController@showDetails');

Route::delete('/delete/{id}')->name('delete')->uses('MovieController@deleteMovie');

Route::get('/editmovieform/{id}')->name('edit-movie-form')->uses('MovieController@showEditMovieForm');

Route::post('/edit/{id}')->name('edit-movie')->uses('MovieController@editMovie');

Route::post('/search')->name('search')->uses('MovieController@searchMovie');
@extends('defaultLayout')

@section('content')
  <div class="container">
     <div class="row">
         <div class="col-md-8 col-md-offset-2">
             <table class="table table-bordered">
                 <thead>
                 <tr>
                     <th>id</th>
                     <th>Title</th>
                     <th>Option</th>
                 </tr>



                 </thead>
                 <tbody>
                 @foreach($movies as $movie)
                     <tr>

                         <td class="col-md-1">{{$movie->id}}</td>
                         <td class="col-md-7">{{$movie->title}}</td>
                         <td class="col-md-4">
                            <div class="col-md-4">
                                <a class="btn btn-primary" href="{{route('movie-details',['id'=> $movie->id])}}">Details</a>
                            </div>
                             <div class="col-md-4">
                                 <a class="btn btn-primary" href="{{route('edit-movie-form',['id'=> $movie->id])}}">Edit</a>
                             </div>
                             <div class="col-md-4">
                                 <form action="{{route('delete',['id'=>$movie->id])}}" method="post">
                                     {{csrf_field()}}
                                     <input hidden  value="delete" name="_method"/>
                                     <input class="btn btn-danger" type="submit" value="Delete"/>
                                 </form>

                            </div>


                         </td>
                     </tr>

                 @endforeach
                 </tbody>
             </table>
             @if($movies instanceof \Illuminate\Pagination\LengthAwarePaginator)
             {{$movies->links()}}
             @endif
         </div>
     </div>

  </div>
@endsection
@extends('defaultLayout')
@section('content')

<div class="container">
    <form action="{{route('add-movie')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label>Title</label>
            <input class="form-control" type="text" name="title"/>
            @foreach($errors->get('title') as $error)
                <p class="text-danger">{{$error}}</p>
            @endforeach
        </div>
        <div class="form-group">
            <label>Year</label>
            <input class="form-control" type="text" name="year"/>
            @foreach($errors->get('year') as $error)
                <p class="text-danger">{{$error}}</p>
            @endforeach
        </div>
        <div class="form-group">
            <label>Director</label>
            <input class="form-control" type="text" name="director"/>
            @foreach($errors->get('director') as $error)
                <p class="text-danger">{{$error}}</p>
            @endforeach
        </div>
        <div class="form-group">
            <label>Actors</label>
            <input class="form-control" type="text" name="actors"/>
            @foreach($errors->get('actors') as $error)
                <p class="text-danger">{{$error}}</p>
            @endforeach
        </div>
        <div class="form-group">
            <label>Poster</label>
            <input type="file" name="poster"/>
            <p class="text-danger">
                @foreach($errors->get('poster') as $error)
                <p class="text-danger">{{$error}}</p>
                @endforeach
            </p>
        </div>
        <div class="form-group">
            <input class="btn btn-primary" type="submit"/>
        </div>
    </form>
</div>

@endsection
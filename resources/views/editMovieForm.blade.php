@extends('defaultLayout')
@section('content')

    <div class="container">
        <form action="{{route('edit-movie',['id'=>$movie->id])}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label>Title</label>
                <input class="form-control" type="text" name="title" value="{{$movie->title}}"/>
            </div>
            <div class="form-group">
                <label>Year</label>
                <input class="form-control" type="text" name="year" value="{{$movie->year}}"/>
            </div>
            <div class="form-group">
                <label>Director</label>
                <input class="form-control" type="text" name="director" value="{{$movie->director}}" />
            </div>
            <div class="form-group">
                <label>Actors</label>
                <input class="form-control" type="text" name="actors" value="{{$movie->actors}}"/>
            </div>
            <div class="form-group">
                <label>Edit Poster</label>
                <input type="file" name="poster"/>
            </div>
            <div class="form-group">
                <input class="btn btn-primary" type="submit"/>
            </div>
        </form>
    </div>

@endsection
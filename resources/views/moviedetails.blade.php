@extends('defaultLayout')

@section('content')
<div class="container">
   <div class="row">
       <div class="col-md-6">
           <h1>{{$movie->title}}</h1>
           <h4>Year : {{$movie->year}}</h4>
           <h4>Director : {{$movie->director}}</h4>
           <h4>Actors : {{$movie->actors}}</h4>


           <a class="btn btn-primary" href="{{route('show-movies')}}">Back</a>
       </div>
       <div class="col-md-6">
           <img src="{{asset($movie->poster)}}" height="500"/>
       </div>
   </div>
</div>


@endsection
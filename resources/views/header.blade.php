<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Movies</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <li><a href="{{route('show-movies')}}">Show Movies</a></li>
                <li><a href="{{route('add-movie-form')}}">Add Movie</a></li>

            </ul>

                <form action="{{route('search')}}" class="navbar-form navbar-right" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input class="form-control" type="text" name="search"/>
                    </div>

                    <input class="btn btn-default" type="submit" value="Search"/>

                </form>



        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<?php

namespace App\Http\Controllers;

use App\CustomStuff\Movie;
use App\CustomStuff\MovieManager;
use App\Http\Requests\AddMovieFormRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class MovieController extends Controller
{

    public function showMovies(){

        $movieManager = new MovieManager();

        $movies = $movieManager->loadMovies();
        return view('showmovies')->with(['movies' => $movies]);
    }

    public function showAddMovieForm(){
        return view('addMovieForm');
    }

    public function addMovie(AddMovieFormRequest $request){

        $movie = $this->getMovieObjectFromRequest($request);

        $movieManager = new MovieManager();

        $movieManager->addMovie($movie);


        return redirect()->route('show-movies');
    }

    public function showDetails($id){
        $movieManager = new MovieManager();

        $movie = $movieManager->getMovieDetails($id);

        return view('moviedetails')->with(['movie'=>$movie[0]]);
    }

    public function deleteMovie($id){
        $movieManager = new  MovieManager();

        $movieManager->deleteMovie($id);

        return redirect()->route('show-movies');
    }

    public function showEditMovieForm($id){
        $movieManager = new MovieManager();

        $movie = $movieManager->getMovieDetails($id);

        return view('editMovieForm')->with(['movie'=> $movie[0]]);
    }

    public function editMovie($id, Request $request){

        $movie = $this->getMovieObjectFromRequest($request);

        $movieManager = new MovieManager();

        $movieManager->editMovie($id,$movie);

        return redirect()->route('show-movies');
    }


    public function searchMovie(Request $request){

        $searchterm = $request->get('search');

        $movieManager = new MovieManager();

        $movies = $movieManager->searchMovie($searchterm);

        return view('showmovies')->with(['movies' => $movies]);

    }

    private function getMovieObjectFromRequest(Request $request){
        $movieData = $request->all();

        $title = $movieData['title'];
        $year = $movieData['year'];
        $director = $movieData['director'];
        $actors = $movieData['actors'];
        if (isset($movieData['poster'])){
            $poster = $movieData['poster'];

            $posterPath = $poster->store('public/posters');

            $posterUrl = \Storage::url($posterPath);
        }else{
            $posterUrl = "";
        }




        $movie = new Movie($title,$year,$director,$actors,$posterUrl);

        return $movie;
    }
}

<?php

namespace App\CustomStuff;

use App\CustomStuff\Movie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class MovieManager

{
    public function loadMovies(){

        $movies = DB::table('movies')->paginate(5);

        return $movies;

    }

    public function addMovie(Movie $movie){

        $movie = $this->getMovieDataArray($movie);

        DB::table('movies')->insert($movie);

    }

    public function getMovieDetails($id){
        $movie = DB::table('movies')->where('id','like',$id)->get();

        return $movie;
    }

    public function deleteMovie($id){
        DB::table('movies')->where('id','like',$id)->delete();
    }

    public function editMovie($id, Movie $movie){

        $movie = $this->getMovieDataArray($movie);

        DB::table('movies')->where('id','like',$id)->update($movie);
    }

    public function searchMovie($searchTerm){
        $movies = DB::table('movies')->where('title','like','%'.$searchTerm.'%')->get();

        return $movies;

    }

    private function getMovieDataArray(Movie $movie){

        $movieDataArray = [
            'title' => $movie->getTitle(),
            'year' => $movie->getYear(),
            'director' => $movie->getDirector(),
            'actors' => $movie->getActors(),
            'poster' => $movie->getPoster()
        ];

        if($movie->getPoster() == "") array_splice($movieDataArray,4 ,1);



        return $movieDataArray;
    }
}
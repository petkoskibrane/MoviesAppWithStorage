<?php

namespace App\CustomStuff;


class Movie
{
    private $title,$year,$director,$actors,$poster;


    public function __construct($title,$year,$director,$actors,$poster)
    {
        $this->title = $title;
        $this->year = $year;
        $this->director = $director;
        $this->actors = $actors;
        $this->poster = $poster;
    }



    public function getTitle()
    {
        return $this->title;
    }


    public function getYear()
    {
        return $this->year;
    }


    public function getDirector()
    {
        return $this->director;
    }

    public function getActors()
    {
        return $this->actors;
    }

    public function getPoster()
    {
        return $this->poster;
    }
}